# NXT onboarding help
## Images
![image.png](./image.png)
## Abbreviations
- **CPO**:	Charge Point Operator.
    - Party that operates and maintains charge points.
- **Clearing House**: 
    - In the EV market it refers to the
process of exchanging information such as transaction information (“CDRs”) for billing (“settling”) and roaming purposes. ?
- **DER**:	Distributed Energy Resources.
- **DSO**:	Distribution System Operator.
    - A netoperator or gridoperator.	?
- **eMIP**:	eMobility Inter-Operation Protocol
- **EMSP**: / eMSP	E-Mobility Service Provider
    - Party that handles all communication and billing
towards EV users.
- **ESI**:	Energy Service Interface	?
- **EV**: Electric Vehicles
- **EVSE**: Electrical Vehicle Supply Equipment 
    - The logical unit in a Charge Point that supplies electric energy via a Connector for charging. An EVSE can have one or multiple Connector(s).
- **IEEE** 2030.5:
    - The IEEE adoption of Smart Energy Application Profile 2.0 (SEP 2) ?
- **OCHP**: Open Clearing House Protocol ?
- **OCHPdirect**: ?
- **OCPI**: Open Charge Point Interface ?
- **OCPP**: Open Charge Point Protocol ?
- **OEM**: Original Equipment Manufacturer
- **OICP**: Open InterCharge Protocol ?
- **OpenADR**: Open Automated Demand Response ?
- **OSCP**: Open Smart Charging Protocol ?
- **Roaming**: 
    - In the telecom industry roaming is the ability of users to make us of their phones/subscriptions beyond the limits of the network of their provider of choice.
    - In the EV domain roaming is very similar: this is what allows EV drivers charge their EV at charging stations that are not part of the charging network of their CPO using the same identification.
- **SEP**: Smart Energy Profile ?
- **Smart Charging**: 
    - Smart charging is when charging an EV can be externally controlled.
- **TSO**: Transmission System Operator?
## Definitions
- **Interoperability**: ?
## Descriptions
- **OpenADR**:
    - As the name implies, the protocol is aimed at automating demand response communication, it
supports a system and / or device to change power consumption or production of demand-side
resources. This can, for example, be done based on grid needs, either by means of tariff and /
or incentives or emergency signals that are intended to balance demand to sustainable supply.
    - Use cases:
        1. Handle registrations (registering a virtual end node at a virtual top node15)
        2. Manage grid
        3. Smart charging
    - Sending price and load control signal, which can be used for decreasing / increasing
power consumption of individual devices, which is a form of managing a (smart) grid.
    - Sending reports. In the EV context this can for example be standardized metering data
from a charge point (for example for monitoring and validating performance), charge
levels (in case of V2G), use times for forecasts etc.
- **OCPI**:
    - The Open Charge Point Interface protocol is designed for exchanging information about charge
points.
    - The protocol is for exchanging information between the market roles of Charge Point
Operator and e-Mobility Service Provider.
    - Use cases:
        1. Providing charge point information
        2. Reservation
        3. Smart charging
        4. Authorizing charging sessions
        5. Roaming
    - Providing charge point information concerns information about location and tariff.
    - Information that can be exchanged concerns smart charging, reservations and
authorization tokens.
- **OCHP**:
    - Is meant for exchanging authorization data, charging transaction and charge point information data for roaming.
    - The protocol consists of 2 parts:
        1. A part that is specifically for communication between market parties and an EV clearing
        house
        2. A part that is for peer to peer communication between market parties, this is called
        OCHPdirect
    - The protocol is currently used with the e-clearing.net clearing house platform, which is operated
by smartlab Innovationsgesellschaft mbH and owned by both smartlab and ElaadNL on an
equal shares basis (non-profit). The version under consideration is 1.4.
    - Use cases:
        1. Authorizing charging sessions
        2. Billing
        3. Providing charge point information
        4. Reservation
        5. Roaming
        6. Smart Charging (only in OCHPdirect, in a basic / lean form)
    - Remote Control of Charge Point (only in OCHPdirect). This includes setting limits, but this is not targeted at dynamic Smart Charging (yet).
    - Providing tariff information
    - Providing Charge Detail Records for billing
    - Providing charging session information (only in OCHPdirect)
- **OCPI**: 
    - Is designed for exchanging information about charge points.
    - The protocol is for exchanging information between the market roles of Charge Point Operator and e-Mobility Service Provider. These roles are not separated in all markets. In some countries or regions these roles are filled in by one party.
    - Use cases: 
        1. Authorizing charging sessions
        2. Billing
        3. Providing charge point information21
        4. Reservation
        5. Roaming
        6. Handle registrations
    - Providing both session information as well as location information.
    - Sending remote commands among which reservation commands.
    - Providing Charge Detail Records for billing purposes
    - Providing tariff information
    - Authorizing charging sessions by exchanging tokens
- **OICP**: 
    - Is a roaming protocol created by Hubject in 2013, which can be used to communicate with the Hubject B2B Service Platform.
    - This platform enables exchanging roaming messages between an EMSP and a CPO.
    - Since 2016 the protocol consists of 2 parts:
        1. A part for the EMSP.
        2. A part for the CPO.
    - Use cases: 
        1. Authorizing charging sessions
        2. Billing
        3. Providing charge point information
        4. Reservation
        5. Roaming
    - Providing Charge Detail Records for billing purposes
    - Providing both session information as well as location information
    - Reserving charge points
    - Sending remote start / stop commands
- **eMIP**:
    - The eMobility Interoperation Protocol (eMIP) is provided by the GIREVE organization.
    - The main objective of GIREVE is: “open access to vehicle charging stations”.
    - The eMIP protocol targets the following goals (from the specification):
        1. Enabling roaming of charging services by providing a charge authorisation and a data clearing house API.
        2. Providing access to a comprehensive charging point database.
        3. Providing smart charging features
    - Use cases:
        1. Authorizing charging sessions
        2. Billing
        3. Providing charge point information
        4. Roaming
        5. Smart charging
    - Providing Charge Detail Records for billing purposes
    - Providing charge point information incl. tariff and parking spot information (static and dynamic)
    - Smart charging functionality ( open to any OEM, currently connected to one OEM)
    - Retrieving list of EVSE's located in a given area and fulfilling a set of criteria (i.e. “search functionality”)
    - Sending event reports and remote commands (emergencystop, stop, suspend and restart)


